import PostCard from "../Postcard";
import './styles.css'

export default function Post({posts}) {
    return(
        <div className="posts">
          {posts.map(post => (
          <PostCard 
            key={post.id}
            cover={post.cover}
            title={post.title}
            body={post.body}
          />
          ))}
        </div>
    )
};
