import './styles.css'

export default function Button({text,handleClick,disabled}) {
    return(
        <button 
        onClick={handleClick}
        className="button"
        disabled={disabled}
        >
            {text}
        </button>
    )
};
