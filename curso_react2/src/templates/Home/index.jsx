import { Component } from 'react';
import './styles.css';
import { loadPosts } from '../../utils/load-posts';
import Post from '../../components/Post';
import Button from '../../components/Button';

class Home extends Component {
  state = {
    posts: [],
    allPosts:[],
    page: 0,
    postPerPage: 2
  };
  componentDidMount() {
    this.loadPosts();
  }
  loadPosts = async () => {
    const  {page,postPerPage} = this.state;
    const postsAndPhotos = await loadPosts();
    this.setState({ 
      posts: postsAndPhotos.slice(page,postPerPage),
      allPosts: postsAndPhotos
     });
  }
  loadMorePosts = () => {
    const{
      posts,
      allPosts,
      page,
      postPerPage
    } = this.state;
    const nextPage = page + postPerPage;
    const nextPosts = allPosts.slice(nextPage,nextPage + postPerPage)
    posts.push(...nextPosts);

    this.setState({ posts, page: nextPage })
  }

  render() {
    const { posts,page,postPerPage,allPosts } = this.state;
    const noMorePosts = page + postPerPage >= allPosts.length;

    return (
      <section className="container">
        <Post posts={posts}/>
        <div className="button-container">
          <Button 
          text="More pages" 
          handleClick={this.loadMorePosts} 
          disabled={noMorePosts}
          />
        </div>
      </section>
    );
  }
}
export default Home;