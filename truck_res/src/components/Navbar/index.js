import logo from "../../assets/logos/logo.png"
import {Link} from 'react-router-dom'
import {FaBars} from 'react-icons/fa'

export default function Navbar(){
    return(
        <nav>
            <Link className="link" to="/">
                <img src={logo} alt=""/>
            </Link>
            <i className="icon"><FaBars/></i>
            <div className="navmenu">
                <Link to="/sobre" activeStyle>
                   Sobre
                </Link>
                <Link to="/sobre" activeStyle>
                   Orçamento
                </Link>
                <Link to="/sobre" activeStyle>
                   Contatos
                </Link>
                <Link to="/sobre" activeStyle>
                   Serviços
                </Link>
            </div>
            <button className="navbtn">
                <Link to="/ligar">Entrar em contato</Link>
            </button>
        </nav>      
    )
}