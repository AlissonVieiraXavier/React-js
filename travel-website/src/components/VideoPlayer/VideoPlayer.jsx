import React, { useState } from "react";
import styled from "@emotion/styled";
import { SvgPlayVideo } from "./svg";
import { IoMdClose } from "react-icons/io";
import { useTranslation } from "react-i18next";

// Estilos para a modal
const ModalWrapper = styled.div`
  display: ${(props) => (props.isOpen ? "grid" : "none")};
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(5, 1fr);
  grid-column-gap: 0px;
  grid-row-gap: 0px;
  position: fixed;
  top: 50%;
  left: 50%;
  width: 300px;
  height: 350px;
  transform: translate(-50%, -50%);
  background-color: ${(props) => props.theme.colors.primary.branco};
  padding: 10px;
  z-index: 1000;

  border-radius: ${(props)=>props.theme.borders.radius.b};
  div:nth-of-type(1){grid-area: 1 / 1 / 2 / 2;
  }
  div:nth-of-type(2){
    
    grid-area: 1 / 2 / 2 / 3;
    text-align: right;
  }
  div:nth-of-type(3){
    
    grid-area: 2 / 1 / 6 / 3;}
  @media screen and (min-width: 768px) {
    top: 50%;
    left: 50%;
    width: 550px;
    height: 450px;
    
  }
`;

// Estilos para o botão de abertura da modal
const OpenModalButton = styled.button`
  cursor: pointer;
  border: none;
  font-size: ${(props) => props.theme.fontes.sizes.h3};
  background: transparent;
  display: flex;
  justify-content: center;
  align-items: center;
  top:-12px;
  position: relative;
  overflow: hidden;
  
`;
const ButtonClose = styled.button`
  cursor: pointer;
  border: none;
  height: 50px;
  max-width: 30px;
  font-size: ${(props) => props.theme.fontes.sizes.h3};
  padding: 0px;
  background: transparent;
  
`;

export const VideoPlayer = () => {
  const [isOpen, setIsOpen] = useState(false);
  const {t} = useTranslation();

  return (
    <>
      <OpenModalButton onClick={() => setIsOpen(true)}>
        {SvgPlayVideo}
      </OpenModalButton>

      <ModalWrapper isOpen={isOpen}>
        <div>
          <p style={{marginLeft:30}}>{t('demo')}</p>
        </div>
        <div>
          <ButtonClose onClick={() => setIsOpen(false)}><IoMdClose /></ButtonClose>
        </div>
        <div>
          <iframe
            width="100%"
            height="100%"
            src="https://www.youtube.com/embed/dQw4w9WgXcQ?si=rHPbqWf_xu62jjJE"
            title="YouTube video player"
            autoPlay
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowFullScreen
          ></iframe>
        </div>
      </ModalWrapper>
    </>
  );
};
