import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const LinkDefault = styled.div`
  margin: auto 10px;
  cursor: pointer;
  display: flex;
  max-height: 52px;
  padding: 20px 5px;
  font-family: ${props => props.theme.fontes.c};
  font-weight: ${props => props.theme.fontes.weights.c};
  line-height: normal;
  a {
    text-decoration: none;
    color: ${props => props.theme.colors.primary.black};
  }
  @media screen and (min-width: 768px) {
   padding: 0px 5px;
   margin: auto 20px;
  }
`;
const LinkWithBorder = styled.div`
  margin: auto 10px;
  cursor: pointer;
  border: 1px solid var(--1st, ${props => props.theme.colors.primary.black});
  padding: 20px 5px;
  font-family: ${props => props.theme.fontes.c};
  font-weight:${props => props.theme.fontes.weights.c};
  line-height: normal;
  a {
    text-decoration: none;
    color: ${props => props.theme.colors.primary.black};
  }
  @media screen and (min-width: 768px) {
    padding: 0px 5px;
    width: 102px;
    height: 40px;
    flex-shrink: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: ${props => props.theme.borders.radius.c};
    margin: auto 20px;
  }
`;

export default function Option({ children, path, border }) {
  return (
    <>
      {border === "solid" ? (
        <LinkWithBorder>
          <Link to={path}>{children}</Link>
        </LinkWithBorder>
      ) : (
        <LinkDefault>
          <Link to={path}>{children}</Link>
        </LinkDefault>
      )}
    </>
  );
}
