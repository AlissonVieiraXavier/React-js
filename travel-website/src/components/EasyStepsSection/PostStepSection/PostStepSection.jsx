import styled from "@emotion/styled";
import Rectangle17 from "../../../assets/Rectangle17.jpg";
import Ellipse3 from "../../../assets/Ellipse3.svg";
import leaf1 from "../../../assets/leaf1.svg";
import send2 from "../../../assets/send2.svg";
import map1 from "../../../assets/map1.svg";
import building from "../../../assets/building.svg";
import heart from "../../../assets/heart.svg";
import image32 from '../../../assets/image32.png'
import Group3 from '../../../assets/Group3.svg'
import { useTranslation } from "react-i18next";

const Container = styled.div`
  display: none;
  align-items: center;
  justify-content: center;
  margin: 50px auto;
  width: 100%;
  z-index: -10;
  position: relative;
  @media screen and (min-width: 765px) {
    display: block;
    width: 40%;  
    top: 100px;
    left: 50px;
  }
`;

const Post = styled.div`
  width: 370px;
  height: 400px;
  flex-shrink: 0;
  border-radius: 26px;
  background: var(--white, ${(props)=>props.theme.colors.primary.branco});
  box-shadow: 0px 1.852px 3.148px 0px rgba(0, 0, 0, 0),
    0px 8.148px 6.519px 0px rgba(0, 0, 0, 0.01),
    0px 20px 13px 0px rgba(0, 0, 0, 0.01),
    0px 38.519px 25.481px 0px rgba(0, 0, 0, 0.01),
    0px 64.815px 46.852px 0px rgba(0, 0, 0, 0.02),
    0px 100px 80px 0px rgba(0, 0, 0, 0.02);
  img {
    width: 321px;
    height: 161px;
    position: relative;
    left: 25px;
    top: 20px;
  }
`;
const Content = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 25px;
`;
const ContentTitle = styled.h5`
  color: var(--black, #080809);
  font-family: ${props => props.theme.fontes.b};
  font-size: 18px;
  text-align: left;
  font-style: normal;
  font-weight: 500;
  line-height: 124.5%; /* 22.41px */
  letter-spacing: 0.27px;
`;
const ContentDescription = styled.div`
  display: flex;
  flex-direction: row;
  position: relative;
  top: -15px;
  div {
    margin-left: 2px;
    color: var(--text-2, #84829a);
    font-family: ${props => props.theme.fontes.b};
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: 124.5%; /* 19.92px */
    letter-spacing: -0.08px;
  }
`;
const ContentIcons = styled.div`
  display: flex;
  width: 139px;
  height: 36px;
  flex-shrink: 0;
  justify-content: space-between;
`;
const Icon = styled.div`
  background: url(${Ellipse3}) center / cover no-repeat;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 36px;
  height: 36px;
  flex-shrink: 0;
  img {
    position: relative;
    left: 1.5px;
    top: 0.5px;
    width: 14px;
    height: 14px;
    flex-shrink: 0;
  }
`;
const ContentFooter = styled.div`
  display: flex;
  justify-content: space-between;
  max-width: 100%;
  height: 20px;
  width: 320px;
  position: relative;
  top: 50px;
`;
const ContentFooterDiv = styled.div`
  display: flex;
  flex-direction: row;
  img {
    width: 20px;
    position: relative;
    left: -5px;
    top: -75px;
  }
`;
const ContentFooterDivP = styled.p`
  color: ${(props)=>props.theme.colors.secondary.gray};
  font-family: ${props => props.theme.fontes.b};
  font-size: ${props => props.theme.fontes.sizes.h7};
  font-style: normal;
  font-weight: ${props => props.theme.fontes.weights.c};
  line-height: 124.5%; /* 19.92px */
  letter-spacing: -0.32px;
  position: relative;
  left: 0px;
  top: -15px;
`;

const Selo = styled.div`
  display: flex;
  flex-direction: row;
  align-items:center;
  padding-left: 20px;
  width: 263px;
  height: 129px;
  flex-shrink: 0;
  border-radius: 18px;
  background: var(--white, ${(props)=>props.theme.colors.primary.branco});
  box-shadow: 0px 1.852px 3.148px 0px rgba(0, 0, 0, 0),
    0px 8.148px 6.519px 0px rgba(0, 0, 0, 0.01),
    0px 20px 13px 0px rgba(0, 0, 0, 0.01),
    0px 38.519px 25.481px 0px rgba(0, 0, 0, 0.01),
    0px 64.815px 46.852px 0px rgba(0, 0, 0, 0.02),
    0px 100px 80px 0px rgba(0, 0, 0, 0.02);
  position: relative;
  top:80px;
  left: -150px;
 img{
    position: relative;
    top:-20px;
    left:-5px;
    border-radius: 100%;
 }
 @media screen and (min-width: 764px){
  position: relative;
  top:-200px;
  left: 220px;
  img{
    position: relative;
    top:-20px;
    left:-5px;
    border-radius: 100%;
 }
 }
`;
const SeloContent = styled.div`
`;
const SeloSubtitle = styled.div`
color: ${(props)=>props.theme.colors.secondary.gray};
font-family: ${props => props.theme.fontes.b};
font-size: ${props => props.theme.fontes.sizes.h8};
font-style: normal;
font-weight:${props => props.theme.fontes.weights.c};
line-height: 124.5%; /* 17.43px */
letter-spacing: -0.49px;
position: relative;
top:20px;
`;
const SeloPorCent = styled.div`
    color: ${(props)=>props.theme.colors.secondary.black};
    font-family: ${props => props.theme.fontes.b};
    font-size: ${props => props.theme.fontes.sizes.h8};
    font-style: normal;
    font-weight: ${props => props.theme.fontes.weights.c};
    line-height: 124.5%;
    letter-spacing: -0.77px;
    position: relative;
    top:-20px;
i{
    color: ${(props)=>props.theme.colors.primary.purple};
}
`;

export const PostStepSection = () => {

  const {t} = useTranslation();

  return (
    <Container>
      <Post>
        <img src={Rectangle17} alt="Rectangle17" />
        <Content>
          <ContentTitle>{t('TripToGreece')}</ContentTitle>
          <ContentDescription>
            <div>14-29 {t('June')} |</div>
            <div>by Robbin joseph</div>
          </ContentDescription>
          <ContentIcons>
            <Icon>
              <img src={leaf1} alt="" />
            </Icon>
            <Icon>
              <img src={map1} alt="" />
            </Icon>
            <Icon>
              <img src={send2} alt="" />
            </Icon>
          </ContentIcons>
          <ContentFooter>
            <ContentFooterDiv>
              <img src={building} alt="building" />
              <ContentFooterDivP>24 {t('PeopleGoing')}</ContentFooterDivP>
            </ContentFooterDiv>
            <ContentFooterDiv>
              <img src={heart} alt="heart" />
            </ContentFooterDiv>
          </ContentFooter>
        </Content>
      </Post>
      <Selo>
        <img src={image32} alt="image32" />
        <SeloContent>
          <SeloSubtitle>{t('Ongoing')}</SeloSubtitle>
          <ContentTitle>{t('TripToGreece')}</ContentTitle>
          <SeloPorCent><i>40% </i>{t('Completed')}</SeloPorCent>
          <img src={Group3} alt="Group3"/>
        </SeloContent>
      </Selo>
    </Container>
  );
};
