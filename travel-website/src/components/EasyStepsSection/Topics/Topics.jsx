import styled from "@emotion/styled";
import Group7 from '../../../assets/Group7.svg';
import Group11 from '../../../assets/Group11.svg';
import Group12 from '../../../assets/Group12.svg';
import { useTranslation } from "react-i18next";

const Container = styled.div`
width: 100%;
display: flex;
flex-direction: column;
align-items: center;
@media screen and (min-width: 765px) {
  display: block;
    width: 50%;
    padding-left: 0%;
    align-items: left;
    justify-content: left;
    max-height: 4774px;
  }
`;

const Subtitle = styled.h5`
  color: ${(props) => props.theme.colors.primary.gray};
  text-align: center;
  font-family: ${(props) => props.theme.fontes.b};
  font-size: ${(props) => props.theme.fontes.sizes.h5};
  font-style: normal;
  font-weight: ${(props) => props.theme.fontes.weights.b};
  line-height: normal;
  position: relative;
  top: 50px;
  z-index: -1;
  margin-left: 3px;
  @media screen and (min-width: 765px) {
    text-align: left;
    padding-left: 0%;
  }
`;
const Maintitle = styled.h2`
  color: ${(props) => props.theme.colors.secondary.blue};
  text-align: center;
  font-family: ${(props) => props.theme.fontes.a};
  font-size: ${(props) => props.theme.fontes.sizes.h2};
  font-style: normal;
  font-weight: ${(props) => props.theme.fontes.weights.b};
  line-height: normal;
  text-transform: capitalize;
  width: 311px;
  height: 130px;
  @media screen and (min-width: 765px) {
    text-align: left;
    padding-top: 20px;
    width: 511px;
  }
`;
const Options = styled.div`
display:flex;
flex-direction: column;
margin-top: 30px;
`;
const Option = styled.div`
 display: flex;
 flex-direction: row;
 align-items: center;
 justify-content: center;
 //border: 1px solid red;
 height: 100px;
 margin-top: 20px;
 img{
  width: 47px;
  height: 48px;
  flex-shrink: 0;
 }
 @media screen and (min-width: 765px) {
  display: flex;
 flex-direction: row;
 align-items: left;
 justify-content: left;
 margin-top: 20px;
  }
`;
const OptionTitle = styled.h6`
color: var(--TEXT-CLR, ${(props)=> props.theme.colors.primary.gray});
font-family: ${(props)=> props.theme.fontes.b};
font-size: ${(props)=> props.theme.fontes.sizes.h7};
font-style: normal;
font-weight: ${(props)=> props.theme.fontes.weights.a};
line-height: 124.5%; /* 19.92px */
padding-left:10px;
z-index:-10;
`;
const OptionText = styled.p`
color: var(--TEXT-CLR, ${(props)=> props.theme.colors.primary.gray});
font-family: ${(props)=> props.theme.fontes.b};
font-size:  ${(props)=> props.theme.fontes.sizes.h7};
font-style: normal;
font-weight: ${(props)=> props.theme.fontes.weights.d};
line-height: 124.5%; /* 19.92px */
padding-left:10px;
position: relative;
top:-30px;
width: 327px;
height: 20px;
z-index:-10;
`;

export const Topics = () => {

  const {t} = useTranslation();

  return (
    <Container>
      <Subtitle>{t('EasyandFast')}</Subtitle>
      <Maintitle>{t('EasyandFastTitle')}</Maintitle>
      <Options>
        <Option>
            <img src={Group7} alt="Group7"/>
            <div>
              <OptionTitle>{t('ChooseDestination')}</OptionTitle>
              <OptionText>{t('Lorem')}</OptionText>
            </div>
        </Option>
        <Option>
            <img src={Group11} alt="Group11"/>
            <div>
              <OptionTitle>{t('MakePayment')}</OptionTitle>
              <OptionText>{t('Lorem')}</OptionText>
            </div>
        </Option>
        <Option>
            <img src={Group12} alt="Group12"/>
            <div>
              <OptionTitle>{t('ReachAirportonSelectedDate')}</OptionTitle>
              <OptionText>{t('Lorem')}</OptionText>
            </div>
        </Option>
      </Options>
    </Container>
  );
};
