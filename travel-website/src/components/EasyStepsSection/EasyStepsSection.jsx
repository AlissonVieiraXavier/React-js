import styled from "@emotion/styled";
import { Topics } from "./Topics/Topics";
import { PostStepSection } from "./PostStepSection/PostStepSection";

const Constainer = styled.div`
 display: flex;
 flex-direction: column;
 @media screen and (min-width: 765px) {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-items: center;
    margin: 20px 10%;
 }
`;


export const EasyStepsSection = () => {
    return(
        <Constainer>
            <Topics/>
            <PostStepSection/>
        </Constainer>
    )
};
