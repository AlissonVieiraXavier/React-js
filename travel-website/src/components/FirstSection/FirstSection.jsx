import styled from '@emotion/styled';
import { VideoPlayer } from "../VideoPlayer/VideoPlayer";
import { BlockTitle } from "./BlockTitle/BlockTitle";
import { Traveler } from "./Traveler/Traveler";

const SectionContainer = styled.section`
  margin-top: 50px;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  @media screen and (min-width: 768px) {
    margin-top: 0px;
  }
`;

export const FirstSection = () => {
    return<SectionContainer>
        <BlockTitle/>
        <Traveler/>
    </SectionContainer>
}

