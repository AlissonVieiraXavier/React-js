import styled from "@emotion/styled"
import plane from '../../../assets/plane.svg';
import girlTraveler from '../../../assets/Traveller.png';

const Container = styled.div`
    display: none;
     @media screen and (min-width: 1172px) {
       display: flex;
       width: 60%;
       z-index: -10;
       margin-top: 0px;
     }
`;

const FirstDiv = styled.div`
    position: absolute;
    left: 40%;
    @media screen and (min-width: 1200px) {
        left: 50%;
     }
    z-index: 1;
    img{
        z-index: 1;
    }
`;
const SecondDivGirl = styled.div`
    position: absolute;
    right: 8%;
    z-index: 2;
    img{
        z-index: 1;
    }
`;
const ThirdDiv = styled.div`
    position: absolute;
    top:25%;
    right: 7%;
    z-index: 0;
    img{
        z-index: 1;
    }
`;

export const Traveler = () => {
    return(
        <Container>
            <FirstDiv><img src={plane} alt="airplane"/></FirstDiv>
            <SecondDivGirl><img src={girlTraveler} alt="a Traveler"/></SecondDivGirl>
            <ThirdDiv><img src={plane} alt="airplane"/></ThirdDiv>
        </Container>
    )
}