/*

      <BtnAction color="">
          Jadoo
      </BtnAction>

*/

import styled from "@emotion/styled";

const ButtonYellow = styled.button`
  border-radius: ${(props) => props.theme.borders.radius.b};
  border: 1px solid;
  font-family: ${(props) => props.theme.fontes.c};
  background: ${(props) => props.theme.colors.primary.yellow};
  font-family: ${(props) => props.theme.fontes.b};
  font-size: ${(props) => props.theme.fontes.sizes.h7};
  line-height: 32px;
  font-style: normal;
  color: ${(props) => props.theme.colors.primary.branco};
  box-shadow: 0px 20px 35px 0px rgba(241, 165, 1, 0.15);
  width: 170px;
  height: 60px;
  flex-shrink: 0;
  font-weight: ${props => props.theme.fontes.weights.c};
`;

const ButtonOrange = styled.button`
  border-radius: ${(props) => props.theme.borders.b};
  border: none;
  font-family: ${(props) => props.theme.fontes.c};
  background: linear-gradient(180deg, #ff946d 0%, #ff7d68 100%);
  font-family: ${(props) => props.theme.fontes.b};
  font-size: ${(props) => props.theme.fontes.sizes.h7};
  line-height: 32px;
  font-style: normal;
  color: ${(props) => props.theme.colors.primary.branco};
  box-shadow: 0px 20px 35px 0px rgba(241, 165, 1, 0.15);
  width: 170px;
  height: 60px;
  flex-shrink: 0;
  font-weight: ${props => props.theme.fontes.weights.c};
`;

export const BtnAction = ({ children, color }) => {
  return (
    <>
      {color === "orange" ? (
        <ButtonOrange>{children}</ButtonOrange>
      ) : (
        <ButtonYellow>{children}</ButtonYellow>
      )}
    </>
  );
};
