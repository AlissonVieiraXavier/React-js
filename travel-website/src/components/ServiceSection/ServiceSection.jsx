import styled from "@emotion/styled";
import { useTranslation } from "react-i18next";
import { Cards } from "./Cards/Cards";

const Container = styled.section`
  display: flex;
  flex-direction: column;
  margin-top: 20px;
  @media screen and (min-width: 765px) {
    margin-top: 80px;
  }
`;
const Subtitle = styled.h5`
  color: ${(props) => props.theme.colors.primary.gray};
  text-align: center;
  font-family: ${(props) => props.theme.fontes.b};
  font-size: ${(props) => props.theme.fontes.sizes.h5};
  font-style: normal;
  font-weight: ${(props) => props.theme.fontes.weights.b};
  line-height: normal;
  position: relative;
  top:60px;
  z-index: -1;
`;

const Maintitle = styled.h2`
  color: ${(props) => props.theme.colors.secondary.blue};
  text-align: center;
  font-family: ${(props) => props.theme.fontes.a};
  font-size: ${(props) => props.theme.fontes.sizes.h2};
  font-style: normal;
  font-weight: ${(props) => props.theme.fontes.weights.b};
  line-height: normal;
  text-transform: capitalize;
`;

export const ServiceSection = () => {

  const { t } = useTranslation();

  return (
  <Container>
        <Subtitle>{t('Category')}</Subtitle>
        <Maintitle>{t('TitleServicesSection')}</Maintitle>
        <Cards/>
  </Container>
  );
};
