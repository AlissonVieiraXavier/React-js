import {ThemeProvider} from '@emotion/react';

const tema = {
    fontes:{
        a: "'Volkhov', serif",
        b:"'Poppins', sans-serif",
        c: "'Open Sans', sans-serif",
        sizes:{
            h1: '84px',
            h2: '50px',
            h3: '33px',
            h4: '20px',
            h5: '18px',
            h6: '17px',
            h7: '16px',
            h8: '14px'
        },
        weights:{
            a: 700,
            b: 600,
            c: 500,
            d: 400,
        }
    },
    colors:{
        background: "#ffffff",
        primary:{
            black: "#212832",
            blue: "#181E4B",
            yellow: "#F1A501",
            orange: "#DF6951",
            gray:"#5E6282",
            branco: "#fff",
            purple: "#8A79DF"
        },
        secondary:{
            black: "#080809",
            blue:"#14183E",
            blue2: "#1E1D4C",
            background: "#FFF1DA",
            gray: "#84829A"
        }
    },
    borders:{
        radius:{
            a: '24px',
            b: '10px',
            c: '5px',
            d: '36px'
        }
    }
}

export const ProvedorTema = ({children}) =>{
    return <ThemeProvider theme={tema}>
        {children}
    </ThemeProvider>
}