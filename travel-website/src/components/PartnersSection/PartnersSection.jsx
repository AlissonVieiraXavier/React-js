import styled from "@emotion/styled";

import img1 from '../../assets/image1.png';
import img2 from '../../assets/image2.png';
import img3 from '../../assets/image3.png';
import img4 from '../../assets/image4.png';
import img5 from '../../assets/image5.png';

const Container = styled.section`
display: flex;
flex-direction:row;
flex-wrap: wrap;
width: 80%;
margin:20px;
justify-content: space-around;
align-items: center;

@media screen and (min-width: 764px){
    margin: 50px 20px 50px 120px;
}

`;

const Partner = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 130px;
    height: 150px;
    flex-shrink: 0;
    img{
        width: 200px;
        max-width: 100px;
        flex-shrink: 0;
    }
`;
const PartnerCustom = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 10px;
    background: var(--white, #FFF);
    box-shadow: 0px 1.852px 3.148px 0px rgba(0, 0, 0, 0.00), 0px 8.148px 6.519px 0px rgba(0, 0, 0, 0.01), 0px 20px 13px 0px rgba(0, 0, 0, 0.01), 0px 38.519px 25.481px 0px rgba(0, 0, 0, 0.01), 0px 64.815px 46.852px 0px rgba(0, 0, 0, 0.02), 0px 100px 80px 0px rgba(0, 0, 0, 0.02);
    width: 241px;
    height: 86px;
    flex-shrink: 0;
    img{
        width: 200px;
        max-width: 100px;
        flex-shrink: 0;
    }
`;

export const PartnersSection = () => {
  return (
    <Container>
      <Partner><img src={img1} alt=""/></Partner>
      <Partner><img src={img2} alt=""/></Partner>
      <PartnerCustom><img src={img3} alt=""/></PartnerCustom>
      <Partner><img src={img4} alt=""/></Partner>
      <Partner><img src={img5} alt=""/></Partner>
    </Container>
  );
};