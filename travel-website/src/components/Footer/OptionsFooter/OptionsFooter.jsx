import styled from "@emotion/styled"

const Container = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: -30px;
    align-items: left;
    @media screen and (min-width: 768px) {
    flex-direction: row;
    margin-left: 30px;
  }
`
const DivLinks = styled.div`
    display: flex;
    flex-direction: column;
    font-family: ${(props) => props.theme.fontes.b};
    margin-left: 50px;
    h5{
        font-size: ${(props) => props.theme.fontes.sizes.h5};
        font-weight: bold;
    }

`
const Options = styled.div`
        display: flex;
        flex-direction: column;
`
const StyledLink = styled.a`
 text-decoration: none;
 color: ${(props) => props.theme.colors.gray};
 opacity: 0.5;
 &.active {
    color: ${(props) => props.theme.colors.gray};
 }
 &.hover{
    color: ${(props) => props.theme.colors.gray};
 }
`;


export const OptionsFooter = () => {
    return(
        <Container>
            <DivLinks>
                <h5>Company</h5>
                <Options>
                    <StyledLink to="/#">About</StyledLink>
                    <StyledLink to="/#">Careers</StyledLink>
                    <StyledLink to="/#">Mobile</StyledLink>
                </Options>
            </DivLinks>
            <DivLinks>
                <h5>Contact</h5>
                <Options>
                    <StyledLink to="/#">Help/FAQ</StyledLink>
                    <StyledLink to="/#">Press</StyledLink>
                    <StyledLink to="/#">Affilates</StyledLink>
                </Options>
            </DivLinks>
            <DivLinks>
                <h5>More</h5>
                <Options>
                    <StyledLink to="/#">Airlinefees</StyledLink>
                    <StyledLink to="/#">Airline</StyledLink>
                    <StyledLink to="/#">Low fare tips</StyledLink>
                </Options>
            </DivLinks>
        </Container>
    )
} 