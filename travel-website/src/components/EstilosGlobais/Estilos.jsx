import {Global} from '@emotion/react';

const estilos = tema => {
    return{
        body:{
            margin: 0,
            overflowX: "hidden",
            background: tema.background,
        }
    }
}


export const Estilos = () =>{
    return(
        <Global styles={estilos}/>
    )
}