// i18n.js
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

const english = require('../lang/translations/en.json')
const espanish = require('../lang/translations/es.json')
const portuguese = require('../lang/translations/pt.json')

// Adicione mais traduções conforme necessário
const resources = {
  en: {
    translation: english,
  },
  es: {
    translation: espanish,
  },
  pt: {
    translation: portuguese,
  },
};

i18n
  .use(initReactI18next) // inicialização para o react-i18next
  .init({
    resources,
    lng: 'en', // idioma padrão
    fallbackLng: 'en', // idioma de fallback
    keySeparator: false, // não use chaves aninhadas
    interpolation: {
      escapeValue: false, // não escapar valores
    },
  });

export default i18n;
