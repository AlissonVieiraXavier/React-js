import { Estilos } from "./components/EstilosGlobais/Estilos";
import { ProvedorTema } from "./components/ProvedorTema/ProvedorTema";
import Home from "./pages/Home/Home";

export default function App() {
  return (
    <ProvedorTema>
      <Estilos />
      <Home/>
    </ProvedorTema>
  );
}
