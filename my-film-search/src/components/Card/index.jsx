import './index.css';
import noPhoto from '../../assets/sem-foto.gif';
import { Link } from 'react-router-dom';

export default function Card({Title,Year,Type,Poster,imdbID}) {
    return(
        <div className="container_card">
            <div className='posterdiv'> <img className="poster" src={Poster && Poster !== "N/A" ? Poster : noPhoto} alt="poster filme"/></div>
            <div className='year' >{Year}</div>
            <div className='title_card' >{Title}</div>
            <div className='type' >{Type}</div>
            {/*<div className='btndiv' ><button className="btn_card"> See Details </button></div>*/}
            <div className='btndiv'><Link className='btn_card' to={`film/${imdbID}`}> See Details </Link></div>
        </div>
    )
};
