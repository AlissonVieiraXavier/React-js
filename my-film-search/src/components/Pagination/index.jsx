import './index.css'
import { Col, Container, Row } from "react-grid-system";
import Card from "../Card";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { setTotalPagesResult, setaAtualPage } from '../../Store/Slices/pagination'
import PageCounter from "../PageCounter";
import { setNoMoreResult } from "../../Store/Slices/search";
import { AiOutlineClear } from "react-icons/ai";

export default function Pagination({Search}) {
    const dispatch = useDispatch();
    const currentPage = useSelector(state => state.pagination.actualPage);
    const totalResultsFromAPI = Search.length;

    const TOTAL_PER_PAGE = 8
    let totalPages = Math.ceil(totalResultsFromAPI / TOTAL_PER_PAGE);
    

    useEffect(() => {
        dispatch(setTotalPagesResult(totalPages))     
    }, []);

    useEffect(() => {
        dispatch(setaAtualPage(currentPage))     
    }, [currentPage]);

    const renderPage = (page) =>{
        const startIndex = (page - 1) * TOTAL_PER_PAGE;
        const endIndex = Math.min(startIndex + TOTAL_PER_PAGE,Search.length)
        const items = Search.slice(startIndex,endIndex)

        return(<Row key={page}>
            {items.map((item,index)=>(
                        <Col key={index} sm={12} md={4} lg={3}>
                            <Card {...item}/>
                        </Col>
                    )
                )
            }
        </Row>)
    }
    const HandleClearButton = () =>{
        dispatch(setNoMoreResult())
    }
      

    return(
        <Container>
            <Row>
                <Col sm={12} md={4} lg={3}><PageCounter/></Col>
                <Col sm={12} md={4} lg={3}>
                    <button 
                        onClick={HandleClearButton} 
                        className="clear_button">
                            <AiOutlineClear /> Clear search
                    </button>
                </Col>
            </Row>
            {renderPage(currentPage)}   
        </Container>
    )
};
