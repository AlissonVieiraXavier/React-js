import './index.css'

export default function Button({width,height,children}) {
    return(
        <button 
            style={{ width: width, height: height}}
            className='button'
            >
             {children}
        </button>
    )
};
