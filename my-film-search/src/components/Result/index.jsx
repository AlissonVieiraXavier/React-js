import { Container } from "react-grid-system";
import Pagination from "../Pagination/index";
//import ResultJson from "./ResultJson";
import { useEffect, useState } from "react";
import './index.css'
import { useDispatch, useSelector } from "react-redux";
import Spinner from "../Spinner";
import { setError } from "../../Store/Slices/search";


export default function Result() {

    const error = useSelector(state => state.search.error);
    const searching = useSelector(state => state.search.searching)
    const resultAPI = useSelector(state => state.search.result)
    const [messageError,setMessageError] = useState("");
    const dispatch = useDispatch();

    useEffect(() => {
        if(resultAPI?.Response == true || resultAPI?.Response == "True"){
            dispatch(setError(false))
            return
        }
        if(resultAPI?.Response == "False"){
          dispatch(setError(true))
          setMessageError(resultAPI.Error)
          return
        }
        if(resultAPI == undefined || resultAPI == false){
          dispatch(setError(true))
          setMessageError("Request limit of API reached!")
        }
        return
    }, [resultAPI?.length > 0]);
    
    return(
        <Container className="resultcamp">
          { error && (
            <div className="no_result">{messageError}</div>
            )
          }
          { searching && (
                <Spinner/>
            )
          }
          {
            resultAPI?.Search && !searching && !error ? (
                <Pagination {...resultAPI}/>
            ):(
              !error && <div className="no_result">No Result, make a search for content...</div>
            )
          }  
        </Container>
    )
};


