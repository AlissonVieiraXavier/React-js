import { createSlice } from "@reduxjs/toolkit";
import ResultJson from "../../components/Result/ResultJson";

const searchSlice = createSlice({
    name: 'searchSlice',
    initialState: {
        searching: false,
        result: ResultJson,
        error: false
    },
    reducers:{
        setSearching: (state) => {
            state.searching = true;
        },
        setNoMoreSearch: (state) => {
            state.searching = false;
        },
        setResult: (state,action) => {
            state.result = action.payload;
            state.searching = false;
        },
        setNoMoreResult: (state) => {
            state.result = false;
        },        
        setError: (state,action) => {
            state.error = action.payload;
        }
    }
});

export default searchSlice.reducer;
export const {setSearching,setNoMoreSearch,setResult,setNoMoreResult,setError} = searchSlice.actions;

