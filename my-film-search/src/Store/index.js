import { configureStore } from '@reduxjs/toolkit';
import search from './Slices/search';
import pagination from './Slices/pagination';

const store = configureStore({
    reducer:{
        search: search,
        pagination: pagination
    }
})
export default store;

